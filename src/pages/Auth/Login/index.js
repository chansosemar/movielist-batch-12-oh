import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {loginAction} from '../authAction';
import {useDispatch, useSelector} from 'react-redux';

import {Tombol, InputData} from '../../../components';

const Login = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const data = [
    {
      title: 'Email',
      data: email,
      setData: setEmail,
    },
    {
      title: 'Password',
      data: password,
      setData: setPassword,
    },
  ];

  const handleSubmitLogin = () => {
    const dataLogin = {
      email: email,
      password: password,
    };
    dispatch(loginAction(dataLogin));
  };

  return (
    <View style={{padding: 20}}>
      <InputData data={data} />
      <Tombol action={handleSubmitLogin} title="LOGIN" />

      <Tombol
        action={() => navigation.navigate('register')}
        title="REGISTER DULU BANG"
      />
    </View>
  );
};

export default Login;
