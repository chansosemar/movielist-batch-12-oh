import {takeLatest, put} from 'redux-saga/effects';
import {registerApi, loginApi} from './authApi';
import {
  loginActionSuccess,
  loginActionFailed,
  registerActionFailed,
  registerActionSuccess,
  resetAuthAction,
} from './authAction';
import {saveToken} from '../../redux/utils';

function* loginAction(action) {
  try {
    console.log(action.payload);
    const res = yield loginApi(action.payload);
    if (res && res.data) {
      console.log('berhasil login');
      console.log(res.data, 'ini Data');
      yield saveToken(res.data.data.token);
      yield put(loginActionSuccess(res.data.data.result));
    } else {
      console.log('gagal login');
      yield put(loginActionFailed());
    }
  } catch (e) {
    console.log(e, 'gagal login');
    yield put(loginActionFailed());
  }
}

function* registerAction(action) {
  try {
    console.log(action.payload, 'payload');
    const res = yield registerApi(action.payload);
    if (res && res.data) {
      console.log('berhasil register');
      yield saveToken(res.data.token);
      yield put(registerActionSuccess(res.data.result));
      yield put(resetAuthAction());
    } else {
      console.log('gagal register');
      yield put(registerActionFailed());
    }
  } catch (e) {
    console.log(e, 'gagal register');
    yield put(registerActionFailed());
  }
}

function* authSaga() {
  yield takeLatest('LOGIN', loginAction);
  yield takeLatest('REGISTER', registerAction);
}

export default authSaga;
