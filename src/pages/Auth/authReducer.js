const initialState = {
  isRegister: false,
  isLogin: false,
  isLoading: false,
  data: [],
  dataRegister: [],
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN': {
      return {
        ...state,
        isLoading: false,
      };
    }
    case 'LOGIN_SUCCESS': {
      return {
        ...state,
        isLogin: true,
        isLoading: false,
        data: action.payload,
      };
    }
    case 'LOGIN_FAILED': {
      return {
        ...state,
        isLoading: false,
      };
    }
    case 'REGISTER': {
      return {
        ...state,
        isLoading: false,
      };
    }
    case 'REGISTER_SUCCESS': {
      return {
        ...state,
        isRegister: true,
        isLoading: false,
        dataRegister: action.payload,
      };
    }
    case 'REGISTER_FAILED': {
      return {
        ...state,
        isLoading: false,
      };
    }
    case 'RESET': {
      return {
        ...state,
        isRegister: false,
        isLogin: false,
        isLoading: false,
        data: [],
      };
    }
    default:
      return state;
  }
};

export default authReducer;
