import React, {useState} from 'react';
import {Text, View, TouchableOpacity, TextInput} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';

import {registerAction} from '../authAction';

import {Tombol, InputData} from '../../../components';

const Register = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [fullName, setFullName] = useState('');
  const [userName, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const data = [
    {
      title: 'Fullname',
      data: fullName,
      setData: setFullName,
    },
    {
      title: 'Username',
      data: userName,
      setData: setUserName,
    },
    {
      title: 'Email',
      data: email,
      setData: setEmail,
    },
    {
      title: 'Password',
      data: password,
      setData: setPassword,
    },
  ];
  const handleSubmitRegister = () => {
    const dataRegister = {
      full_name: fullName,
      username: userName,
      email: email,
      password: password,
      profile_picture: 'ini url',
    };
    dispatch(registerAction(dataRegister));
  };
  return (
    <View style={{padding: 20}}>
      <InputData data={data} />
      <Tombol action={handleSubmitRegister} title="REGISTER" />

      <Tombol
        action={() => navigation.navigate('login')}
        title="SUDAH PUNYA AKUN YUK LOGIN"
      />
    </View>
  );
};

export default Register;
