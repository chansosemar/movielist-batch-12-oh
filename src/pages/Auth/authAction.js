export const loginAction = payload => {
  return {type: 'LOGIN', payload};
};
export const loginActionSuccess = payload => {
  return {type: 'LOGIN_SUCCESS', payload};
};
export const loginActionFailed = payload => {
  return {type: 'LOGIN_FAILED', payload};
};
export const registerAction = payload => {
  return {type: 'REGISTER', payload};
};
export const registerActionSuccess = payload => {
  return {type: 'REGISTER_SUCCESS', payload};
};
export const registerActionFailed = payload => {
  return {type: 'REGISTER_FAILED', payload};
};
export const resetAuthAction = payload => {
  return {type: 'RESET_AUTH'};
};
