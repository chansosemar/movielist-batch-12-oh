import axios from 'axios';

export const registerApi = payload => {
  console.log(payload, 'dariaxios');
  return axios({
    method: 'POST',
    url: 'https://rocky-beyond-12209.herokuapp.com/api/users/register',
    data: payload,
  });
};

export const loginApi = payload => {
  console.log(payload, 'dariaxios');
  return axios({
    method: 'POST',
    url: 'https://rocky-beyond-12209.herokuapp.com/api/users/login',
    data: payload,
  });
};
