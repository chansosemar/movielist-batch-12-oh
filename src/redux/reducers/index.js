import {combineReducers} from 'redux';
import authReducer from 'pages/Auth/authReducer';
import homeReducer from 'pages/HomePage/homeReducer';

export default combineReducers({authReducer, homeReducer});
