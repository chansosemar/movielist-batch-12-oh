import {all} from 'redux-saga/effects';
import homeSaga from 'pages/HomePage/homeSaga';
import authSaga from 'pages/Auth/authSaga';

export default function* rootSaga() {
  yield all([homeSaga(), authSaga()]);
}
