import React from 'react';
import {Text, TextInput, View} from 'react-native';

const InputData = ({data}) => {
  return (
    <>
      {data.map((e, i) => (
        <View key={i}>
          <TextInput
            placeholder={e.title}
            value={e.data}
            onChangeText={text => e.setData(text)}
          />
        </View>
      ))}
    </>
  );
};

export default InputData;
