import React from 'react';
import {Text, TouchableOpacity} from 'react-native';

const Tombol = ({action, title}) => {
  return (
    <TouchableOpacity onPress={action}>
      <Text>{title}</Text>
    </TouchableOpacity>
  );
};

export default Tombol;
